# Uexec

Uexec automatically executes commands as storage devices become
available, i.e. are plugged in.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'uexec'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install uexec

## Usage

Uexec provides a script `uexec`.

With the `monitor` subcommand you can watch the dbus events emitted by
plugging and unplugging storager devices.

    $ uexec monitor

This will eventually create the following directory structure:

    $ tree ~/.uexec
    /home/user/.uexec
    ├── config.yml
    ├── output
    │   ├── AX216-FLASH-READER-AX216_FLASH_READER-0:0.out
    │   └── Multiple-Card--Reader-058F63666438.out
    └── properties
        ├── AX216-FLASH-READER-AX216_FLASH_READER-0:0.yml
        └── Multiple-Card--Reader-058F63666438.yml

    2 directories, 5 files...

You can also list the already detected devices with the `list`
subcommand.

    $ uexec list

With the `alias` subcommand you can alias the device's id to something
more rememberable. If an alias is configured the files in
`~/.uexec/properties` and `~/.uexec/output` will be named by it,
instead of the device's id.

    $ uexec alias <device-id> <new-name>

If you want to run `uexec` as a service on your system you can use
`install` to set up a proper systemd service.

    $ sudo uexec install --user=$USER

For now you will also have to install `ruby-dbus` via the package
manager of your system, e.g.

    $ sudo apt install ruby-dbus

## Development

Run `rake spec` to run the tests.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will
create a git tag for the version, push git commits and tags, and push
the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://github.com/branch14/uexec.
