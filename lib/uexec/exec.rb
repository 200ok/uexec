module Uexec
  # provides the interface for the executable in bin
  class Exec

    attr_accessor :args

    def initialize(args)
      self.args = args
    end

    def call
      opts = Hash[args.join(' ').scan(/--?([^=\s]+)(?:=(\S+))?/)]
      subcmd = args.first
      case subcmd
      when nil then raise 'Missing subcommand'
      when 'monitor'
        require File.expand_path(File.join(%w(.. monitor)), __FILE__)
        Monitor.new.call
      when 'list'
        require File.expand_path(File.join(%w(.. list)), __FILE__)
        List.new.call
      when 'install'
        require File.expand_path(File.join(%w(.. install)), __FILE__)
        Install.new(opts).call
      else raise "Unknown subcommand: #{subcmd}"
      end
    rescue StandardError => e
      puts e.message
      exit 1
    end
  end
end
