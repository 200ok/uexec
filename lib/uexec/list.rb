require File.expand_path(File.join(%w(.. config)), __FILE__)

module Uexec
  class List
    def call
      entries = Config.new.read
      len = entries.map { |e| e.first.length }.max
      entries.each do |key, value|
        puts "%-#{len}s %s" % [key, value[:description]]
      end
    end
  end
end
