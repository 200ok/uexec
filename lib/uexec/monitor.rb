require 'dbus'
require 'yaml'
require 'pp'

require File.expand_path(File.join(%w(.. config)), __FILE__)

module Uexec
  class Monitor

    def call
      @bus = DBus::SystemBus.instance
      service = @bus.service('org.freedesktop.UDisks2')
      object = service.object('/org/freedesktop/UDisks2')
      object.default_iface = 'org.freedesktop.DBus.ObjectManager'

      # https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces-objectmanager
      object.on_signal('InterfacesAdded') do |signal|
        puts "-> ADD #{signal}"
        begin
          obj = service.object(signal)
          props = get_all_props(obj, 'org.freedesktop.UDisks2.Drive')
        # puts YAML.dump(props)
        rescue StandardError => e
          puts "Failed to fetch properties for #{signal}: #{e.message.split(';').first}"
        end
        handle(props) if signal =~ %r{^/org/freedesktop/UDisks2/drives/}
      end

      object.on_signal('InterfacesRemoved') do |signal|
        puts "-> REM #{signal}"
      end

      run_main_loop
    end

    private

    def run_main_loop
      puts 'Entering dbus main loop...'
      loop = DBus::Main.new
      loop << @bus
      loop.run
    end

    def get_all_props(obj, iface)
      obj['org.freedesktop.DBus.Properties'].GetAll(iface).first
    end

    def handle(props)
      config = Config.new.read

      id = props['Id']
      return unless id

      puts "-> ID  #{id}"

      if (entry = config[id])
        if (cmd = entry[:cmd])
          output = %x[#{cmd}]
        end
      else
        entry = { cmd: 'date', description: 'no description' }
      end
      entry[:last] = Time.now

      # write config
      config[id] = entry
      Config.new.write(config)

      # write outfile
      path = File.expand_path("#{id}.out", Config::OUTPUT_PATH)
      write(path, output)

      # write props file
      path = File.expand_path("#{id}.yml", Config::PROPS_PATH)
      write(path, YAML.dump(props))
    rescue StandardError => e
      puts "Failed to handle properties for #{id}: #{e.message.split(';').first}"
    end

    def write(filename, data)
      puts "Writing #{filename}"
      File.open(filename, 'w') { |f| f.write(data) }
    end

  end
end
