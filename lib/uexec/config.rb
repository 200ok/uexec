require 'yaml'

module Uexec
  class Config

    BASE_PATH = File.join(ENV['HOME'], '.uexec')
    OUTPUT_PATH = File.join(BASE_PATH, 'output')
    PROPS_PATH = File.join(BASE_PATH, 'properties')

    CONFIG_FILE = File.join(BASE_PATH, 'config.yml')

    def initialize
      Dir.mkdir(BASE_PATH) unless Dir.exist?(BASE_PATH)
      Dir.mkdir(OUTPUT_PATH) unless Dir.exist?(OUTPUT_PATH)
      Dir.mkdir(PROPS_PATH) unless Dir.exist?(PROPS_PATH)
    end

    def read
      if File.exist?(CONFIG_FILE)
        YAML.load(File.read(CONFIG_FILE))
      else
        {}
      end
    end

    def write(data)
      File.open(CONFIG_FILE, 'w') { |f| f.write(YAML.dump(data)) }
    end

  end
end
