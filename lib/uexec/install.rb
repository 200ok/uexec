require 'erb'
require 'ostruct'
require 'pp'

module Uexec
  # responsible for installing uexec as systemd service
  class Install < Struct.new(:opts)
    def call
      check!
      configure!
      install!
    end

    private

    def check!
      raise 'Install needs to be run as root' unless Process.euid.zero?
      raise 'You need to provide the user with `--user=<user>`' unless user
    end

    def configure!
      File.open(target, 'w') { |f| f.write(service) }
    end

    def target
      "/lib/systemd/system/uexec-#{user}.service"
    end

    def user
      opts['user']
    end

    def service
      ERB.new(template).result(ErbBinding.new(settings).get_binding)
    end

    def template
      File.read(source)
    end

    def source
      File.expand_path(File.join(%w(.. .. .. uexec.service.erb)), __FILE__)
    end

    def settings
      {
        user: user,
        home: File.expand_path("~#{user}"),
        runner: File.expand_path(File.join(%w(.. .. .. bin uexec)), __FILE__)
      }
    end

    def install!
      %x[systemctl daemon-reload]
      %x[systemctl enable uexec-#{user}.service]
      %x[systemctl start uexec-#{user}.service]
      puts 'Uexec is now running as a service.'
    end
  end

  # easy access from within an erb template
  class ErbBinding < OpenStruct
    def get_binding
      return binding()
    end
  end
end
